# PuppyPic API
This is the back end API layer for the PuppyPic application. It is a spring boot application exposing a set RESTful web services. The following methods are available:

1. **GET /** - List of pictures with scores
1. **GET /{id}** - Get a picture and its score by id
1. **GET /{id}/up** - Increases the score of a picture (by id) by 1 and returns the updated object
1. **GET /{id}/down** - Decreases the score of a picture (by id) by 1 and returns the updated object
1. **DELETE /{id}** - Deletes a picture by id
1. **PUT /** - Adds a picture to the database and returns the newly created object 

# Input
* You are provided with the source code of the application
* Sample config files are available in the `src/main/resources` folder
* A build script is available at `build.sh`

# Output
* Build artifacts are located in the `build` directory after build
* The build artifact will be named `api.jar`
* `api.jar` is an executable jar
* A database will need to be created and configured
* This application is unnecessarily verbose. The logging configuration will need to be fixed to be sane.

# Notes
* Spring boot uses convention over configuration. As such, application configuration can be created in a number of convenient ways
  * You may choose to create a configuration file
  * You may choose to use environment variables to define the application configuration
  * You may pass system properties during application startup
