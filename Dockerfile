FROM maven:alpine as build
COPY . /app
WORKDIR /app
RUN mvn clean package

FROM openjdk:alpine
COPY --from=build /app/build /app
WORKDIR /app
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "api.jar" ]