package ai.leverton.devtest.puppypic.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.web.bind.annotation.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Puppy(val url: String, var score: Int = 0, @Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long = -1)

interface PuppyRepository : JpaRepository<Puppy, Long>

@RestController
@RequestMapping("/")
class IndexController {
  @Autowired
  lateinit var repo: PuppyRepository

  @GetMapping
  fun list(): Array<Puppy> = repo.findAll().toTypedArray()

  @GetMapping("/{id}")
  fun one(@PathVariable id: Long): Puppy = repo.findOne(id)

  @GetMapping("/{id}/up")
  fun up(@PathVariable id: Long): Puppy {
    val p = repo.findOne(id)
    p.score ++
    return repo.save(p)
  }

  @GetMapping("/{id}/down")
  fun down(@PathVariable id: Long): Puppy {
    val p = repo.findOne(id)
    p.score --
    return repo.save(p)
  }

  @DeleteMapping("/{id}")
  fun delete(@PathVariable id: Long) {
    repo.delete(id)
  }

  @PutMapping
  fun add(@RequestBody puppy: Puppy) = repo.save(puppy)
}