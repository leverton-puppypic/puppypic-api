package ai.leverton.devtest.puppypic
import ai.leverton.devtest.puppypic.api.PuppyRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

fun main(args: Array<String>) {
  SpringApplication.run(App::class.java, *args).start()
}
@SpringBootApplication
open class App {
  companion object {
    val logger: Logger = LoggerFactory.getLogger("user")
  }
  @Bean
  open fun init(repo: PuppyRepository) = CommandLineRunner{
    logger.info("Starting User Service...")
  }
}
@Component
class ApplicationStartup: ApplicationListener<ApplicationReadyEvent> {
  override fun onApplicationEvent(p0: ApplicationReadyEvent?) {
    App.logger.info("Started User Service")
  }
}
